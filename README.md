# A Perception-Based Nanosensor Platform to Detect Cancer Biomarkers

Code and files presented in this repository are for reproducing the input feature vector construction and machine learning model training/testing in [Z. Yaari, Y. Yang, E. Apfelbaum, C. Cupo, A. Settle, Q. Cullen, W Cai, K. L. Roche, D. A. Levine, M. Fleisher, L. Ramanathan, M. Zheng, A. Jagota, D. A. Heller, (2021)](https://doi.org/00000).

Note that all codes are in **codes** directory. The codes (\*.R) are written in R, and (\*.py) are written in python. 


## Input feature construction
We tested two different feature vector (FV) methods to represent experimentally measured data matrices composed of DNA sequences and SWCNT chiralities. 
Each vector was constructed with two components: "Example ID" – SWCNT chirality or DNA sequence, and "Features" – the DNA-SWCNT complex emission intensity and wavelength response. 
In addition, the vector corresponds to a specific label that indicates the presence of each biomarker in the sample. 
FV1 is focused on chirality and uses DNA sequences as the example IDs and chirality-dependent optical responses as features. DNA sequences were encoded into IDs as either bi-gram or tri-gram term frequency vectors.  
FV2 uses chiralities as the example IDs combined with sequence-dependent optical responses as features. 
The created feature vector files are saved in **data** directory and the sample feature vectors are in **data/featurevector_samples**.

- Sample experimental dataset are provided: 
	- `training_analytes_spectroscopic_data.xlsx` contains the DNA-SWCNT-analyte complex emission intensity and wavelength response. This is used for training. 
	- `test_patient_sample_spectroscopic_data_PBSctrl.xlsx` contains the DNA-SWCNT-{patient sample} complex emission intensity and wavelength response with PBS control sample. This is used for test the trained model.
	- `test_patient_sample_spectroscopic_data_PBSctrl.xlsx` contains the DNA-SWCNT-{patient sample} complex emission intensity and wavelength response with patient biofluid control sample. This is used for test the trained model.
	
- Sample feature vector sets are provided:
	- `fv1_3gram.csv` contains FV1 (DNA sequences were translated using tri-gram term frequency vector) and anlaytes concentrations (label)
	- `fv2_seq.csv` contains FV2 (categorical values were translated using one-hot encoding) and anlaytes concentrations (label)
	- `PCA_using_fv1_3gram.csv` contains the first five principal components estimated using FV1 (tri-gram tfv) and anlaytes concentrations (label)
	- `PCA_using_fv2_seq.csv` contains the first five principal components estimated using FV2 and anlaytes concentrations (label)
	...

- `preprocess_data.R` creates feature vectors (FV1 and FV2) from the raw data which has the spectroscopic measurements data (wavelentgh and intensity) for each DNA/SWCNT/analytes(HE4/CA125/YKL40) complex.
	- Replace `pattern` in line 130 to create trainig/test feature vector
	- Replace the function name in line 227 if you use different n-gram (uni/di/tri).

- `preprocess_pca.R` performs Principal Component Analysis (PCA) using the feature vectors (FV1 and FV2) then plot the PCA componets and save the results in **results** directory. The sample plots are in **results/samples-PCAplot**. The PCs can be used as input features to train ML model.
	- Replace `Npc` in line 46 to change the number of PCA components


## Train ML models and hyperparameter optimization 
Train multiple models with different multi-label/class algorithm using Random Forest, Artifitial Neural Network, and Support Vector Machine. 
All machine learning algorithms were implemented using the [Scikit-learn machine learning library](https://jmlr.csail.mit.edu/papers/v12/pedregosa11a.html). Bayesian hyperparameter optimization was implemented using [HyperOpt](https://dl.acm.org/doi/10.5555/3042817.3042832).
Hyperparameter optimization can be examined by showing the optimization process as a function of iteration (set `plot = True` at the beginning of the main script).
The trained models and performance summary are saved in **results/{model type}** directory. The sample models and results are in **results/samples-MLresults**.

- `multilabel_AA.py` train the multi-label classification models using two different algorithms (Random Forest and Artificial Neural Network). The hyperparameters of each model are optimized using Bayesian Optimization (implemented by hyperOpt) and the optimized models are saved at the end.
	
- `multilabel_BR.py` train the multi-label classification models using three different algorithms (Random Forest, Artificial Neural Network, and Support Vector Machine) with Binary Relevance method. The hyperparameters of each model are optimized using Bayesian Optimization (implemented by hyperOpt) and the optimized models are saved at the end.
	
- `multilabel_LP.py` train the multi-label classification models using three different algorithms (Random Forest, Artificial Neural Network, and Support Vector Machine) with Label Powerset method. The hyperparameters of each model are optimized using Bayesian Optimization (implemented by hyperOpt) and the optimized models are saved at the end.
	
- `multilabel_regression.py` train the multi-label regression models using two different algorithms (Random Forest and Artificial Neural Network). The hyperparameters of each model are optimized using Bayesian Optimization (implemented by hyperOpt) and the optimized models are saved at the end.


## Feature importance analysis
The calculated feature importance is saved in **results/{model type}/fi** directory.

- `calc_fi.py` calculate the feature importance of the trained Random Forest classification/regression models. 


 

## Test ML models using the patient biofluid samples
The prediction probability results are saved in **results/{model type}/test_{control sample type}** directory.

- `test_MLmodels` predict the analyte concentrations/classes, then return the prediction probability

