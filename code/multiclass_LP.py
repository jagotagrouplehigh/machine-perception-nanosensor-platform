""" This script reads the feature vectors and train the multi-label classification models using three different
algorithms (Random Forest, Artificial Neural Network, and Support Vector Machine) with Label Powerset method.
The hyperparameters of each model are optimized using Bayesian Optimization (implemented by hyperOpt)
and the optimized models are saved at the end. """

""" Import Libraries """
from glob import glob
import pandas as pd
import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn import model_selection
from sklearn.metrics import make_scorer, f1_score
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials, space_eval
from sklearn.externals import joblib
from pathlib import Path
from matplotlib import pyplot as plt
from sklearn.model_selection import StratifiedShuffleSplit
from skmultilearn.problem_transform import LabelPowerset
from matplotlib.axes._axes import _log as matplotlib_axes_logger
matplotlib_axes_logger.setLevel('ERROR')


""" Set function to optimize model """
def optimize_model(objective, space, MAX_EVALS):
    """
    This function find the best model with a given hyperparameter space and objective function.
    objective: objective function to minimize
    space: hyperparameter space to optimize
    MAX_EVALS: maximum number of the optimization run
    """
    trials = Trials()
    best_model = fmin(fn=objective,
                      space=space,
                      algo=tpe.suggest,
                      max_evals=MAX_EVALS,
                      trials=trials)
    return best_model, trials


""" Objective functions for Label Powerset """
def objective_LP_RF(params):
    """
    Objective function for RandomForest hyperparameter tuning
    params: model hyperparameter set
    """
    clf = LabelPowerset(RandomForestClassifier(**params,
                                               random_state=0,
                                               n_jobs=-1))

    cv_temp = model_selection.cross_val_score(clf,
                                              fv_train,
                                              class_train,
                                              cv=cv,
                                              scoring=make_scorer(f1_score, average='weighted'),
                                              n_jobs=-1)
    loss = 1 - cv_temp.mean()
    return {'loss': loss, 'params': params, 'status': STATUS_OK}

def objective_LP_ANN(params):
    """
    Objective function for Artifitial Neural Network hyperparameter tuning
    params: model hyperparameter set
    """
    clf = LabelPowerset(MLPClassifier(**params,
                                      max_iter=500,
                                      solver='adam',
                                      random_state=0))


    cv_temp = model_selection.cross_val_score(clf,
                                              fv_train,
                                              class_train,
                                              cv=cv,
                                              scoring=make_scorer(f1_score, average='weighted'),
                                              n_jobs=-1)
    loss = 1 - cv_temp.mean()
    return {'loss': loss, 'params': params, 'status': STATUS_OK}

def objective_LP_SVM(params):
    """
    Objective function for Support Vector Machine hyperparameter tuning
    params: model hyperparameter set
    """
    clf = LabelPowerset(SVC(**params,
                            random_state=0))

    cv_temp = model_selection.cross_val_score(clf,
                                              fv_train,
                                              class_train,
                                              cv=cv,
                                              scoring=make_scorer(f1_score, average='weighted'),
                                              n_jobs=-1)
    loss = 1 - cv_temp.mean()
    return {'loss': loss, 'params': params, 'status': STATUS_OK}


def param_space(model, fvsize):
    if 'RF' in str(model):
        # hyperparameters space for Ramdom Forest
        space = {
            'max_depth': hp.choice('max_depth', range(1, 50)),
            'max_features': hp.choice('max_features', range(np.ceil(fvsize / 2).__int__(), fvsize)),
            'n_estimators': hp.choice('n_estimators', range(50, 150)),
            'criterion': hp.choice('criterion', ['gini', 'entropy']),
            'min_samples_split': hp.loguniform('min_samples_split', np.log(1e-4), np.log(0.02)),
            'min_samples_leaf': hp.loguniform('min_samples_leaf', np.log(1e-5), np.log(0.1))
        }
    elif 'ANN' in str(model):
        # hyperparameters space for Artificial Neural Network
        space = {
            'hidden_layer_sizes': hp.choice('hidden_layer_sizes',
                                            range(np.ceil((fvsize + 2) / 2 - 5).__int__(),
                                                  np.ceil((fvsize + 2) / 2 + 5).__int__())),
            'activation': hp.choice('activation', ['relu',
                                                   'logistic',
                                                   'tanh']),
            'alpha': hp.loguniform('alpha', np.log(1e-2), np.log(0.06)),
            'learning_rate': hp.choice('learning_rate', ['constant', 'adaptive']),
            'learning_rate_init': hp.loguniform('learning_rate_init', np.log(1e-2), np.log(0.1))
        }
    elif 'SVM' in str(model):
        # hyperparameters space for Support Vector Machine
        space = {
            'C': hp.loguniform('C', np.log(10 ** (-2)), np.log(1e3)),
            'kernel': hp.choice('kernel', ['rbf', 'poly']),
            'gamma': hp.loguniform('gamma', np.log(10 ** (-3)), np.log(20))
        }
    return space


def plot_opt(paramlist, trials, savename):
    """
    (optional)
    show the optimization process by plotting each hyperparameter during optimization
    """
    cols = len(paramlist)
    fig, axes = plt.subplots(nrows=2, ncols=cols, figsize=(cols * 3, 6.5))
    cmap = plt.cm.jet
    f1_list = pd.DataFrame(data=[])
    for i, par in enumerate(paramlist):
        pars = np.array([t['misc']['vals'][par] for t in trials.trials]).ravel()
        f1s = [1 - t['result']['loss'] for t in trials.trials]

        axes[0, i].scatter(pars, f1s, s=20, linewidth=0.01, alpha=0.75, c=cmap(float(i) / cols))
        axes[0, i].set_title(par)
        axes[0, i].set_xlabel(par)
        axes[0, i].set_ylabel('f1-score')
        f1_list[par] = pars
        f1_list['f1_'+par] = f1s

        tidx = [t['tid'] for t in trials.trials]
        axes[1, i].scatter(tidx, pars, s=20, linewidth=0.01, alpha=0.75, c=cmap(float(i) / cols))
        axes[1, i].set_title(par)
        axes[1, i].set_xlabel('# of iteration')
        axes[1, i].set_ylabel(par)

    plt.show()
    fig.savefig(savename, dpi=300)
    f1_list.to_csv(savename + '.csv')


verbose = True
# If you want to see the hyperparameter optimization details, you can plot the hyperparameter vs. iteration
plot = True

""" ======================================== main script ======================================== """

""" Load data """
# list input data filenames (feature vectors and labels)
filenames = list(set(glob('../data/fv*')) - set(glob('../data/*ctrl*')))

# set scoring factor for optimization
scoring = 'f1_weighted'    # weighted f1-score

# set cutoff concentration to define class
cutoff_HE4 = 0.01
cutoff_CA125 = 0.01
cutoff_YKL40 = 0.01

# set path to save the optimized models and results
cutoffstr = 'HE4_{}_CA125_{}_YKL40_{}/'.format(cutoff_HE4, cutoff_CA125, cutoff_YKL40).replace('.','p')
savepth = '../results/multilabel-LP_cutoff-' + cutoffstr
Path(savepth).mkdir(parents=True, exist_ok=True)
Path(savepth + '/models').mkdir(parents=True, exist_ok=True)
Path(savepth + '/figs').mkdir(parents=True, exist_ok=True)

# set 10-fold cross-validation
cv = StratifiedShuffleSplit(n_splits=10, test_size=0.1, random_state=0)

# create an empty dataframe to save f-score values
list_fscore = pd.DataFrame(data=[])

# set maximum number of optimization runs
MAX_EVALS = 10


for filename in filenames:
    # read input data
    df = pd.read_csv(filename)
    # extract feature vector type to set the savename later
    trfvtype = filename.split('/')[-1].split('.')[0]

    """ Split data into features and classes """
    df_temp = df
    # index of column for class (label)
    idx_class = [i for i, s in enumerate(df.columns) if '_' in s][-1] + 1

    # extract feature vector only
    fv_train = df_temp.iloc[:, 0:idx_class]

    # normalize the columns so that each value is between -1 and 1
    fv_train = fv_train.apply(lambda x: (x - x.min()) / (x.max() - x.min()) * 2 - 1, axis=0)

    # extract class (label) only
    class_train = df_temp[['HE4', 'CA125', 'YKL40']]

    if verbose:
        print('feature vectors and classes(labels) were extracted from the file ({})'.format(filename))

    # set classes based on the cutoff concentration
    class_train['HE4'].values[class_train['HE4'].values < cutoff_HE4] = 0
    class_train['HE4'].values[class_train['HE4'].values >= cutoff_HE4] = 1
    class_train['CA125'].values[class_train['CA125'].values < cutoff_CA125] = 0
    class_train['CA125'].values[class_train['CA125'].values >= cutoff_CA125] = 1
    class_train['YKL40'].values[class_train['YKL40'].values < cutoff_YKL40] = 0
    class_train['YKL40'].values[class_train['YKL40'].values >= cutoff_YKL40] = 1

    # set class variable tyte as int
    class_train = class_train.astype('int')

    if verbose:
        print('classes are defined based on the cutoff concentrations \n'
              '( HE4: {},    CA125: {},    YKL40: {} )'.format(cutoff_HE4, cutoff_CA125, cutoff_YKL40))

    """ Hyperparameter optimization using Bayesian optimization """
    # The objective function is what we are trying to minimize.
    # We use (1 - 'F-score') as the objective function.

    # ================================= Random Forest ================================ #
    if verbose:
        print('optimizing Random Forest...')
    # Find the best model by optimizing hyperparameters
    best_RF_temp, trials_RF = optimize_model(objective_LP_RF,
                                             param_space('RF', fv_train.shape[1]),
                                             MAX_EVALS)
    # Convert parameter space index to actual parameter space
    best_RF_param = space_eval(param_space('RF', fv_train.shape[1]), best_RF_temp)
    # Evaluate the best model
    best_RF = LabelPowerset(RandomForestClassifier(**best_RF_param, random_state=0))
    best_RF_scores = model_selection.cross_validate(best_RF,
                                                    fv_train,
                                                    class_train,
                                                    cv=cv,
                                                    scoring=scoring,
                                                    return_train_score=False,
                                                    n_jobs=-1)

    # cross-validation results (dict) to dataframe
    best_RF_scores_df = pd.DataFrame.from_dict(best_RF_scores).drop(['fit_time', 'score_time'], axis=1)

    if verbose:
        print('done.. \n{} is {}'.format(scoring, best_RF_scores_df.mean()['test_score']))

    # set save filename
    savename = savepth + 'models/' + trfvtype + '_best_LP_RF.pkl'
    # save the optimized model
    joblib.dump(best_RF, savename)

    # store F-score to the list to summarize the results at the end
    list_fscore = list_fscore.append(pd.DataFrame(index=[trfvtype + '_LP_RF'], data=[best_RF_scores_df.mean()]))

    if plot:
        figname = savepth + 'figs/' + trfvtype + '_best_LP_RF'
        plot_opt([*best_RF_param], trials_RF, figname)

    ## =========================== Artificial Neural Network ========================== #
    if verbose:
        print('optimizing Artificial Neural Network...')
    # Find the best model by optimizing hyperparameters
    best_ANN_temp, trials_ANN = optimize_model(objective_LP_ANN,
                                               param_space('ANN',
                                                           fv_train.shape[1]),
                                               MAX_EVALS)
    # Convert parameter space index to actual parameter space
    best_ANN_param = space_eval(param_space('ANN', fv_train.shape[1]), best_ANN_temp)
    # Evaluate the best model
    best_ANN = LabelPowerset(MLPClassifier(**best_ANN_param,
                                           max_iter=500,
                                           solver='adam',
                                           random_state=0))
    best_ANN_scores = model_selection.cross_validate(best_ANN,
                                                     fv_train,
                                                     class_train,
                                                     cv=cv,
                                                     scoring=scoring,
                                                     return_train_score=False,
                                                     n_jobs=-1)

    # cross-validation results (dict) to dataframe
    best_ANN_scores_df = pd.DataFrame.from_dict(best_ANN_scores).drop(['fit_time', 'score_time'], axis=1)

    if verbose:
        print('done.. \n{} is {}'.format(scoring, best_ANN_scores_df.mean()['test_score']))

    # set save filename
    savename = savepth + 'models/' + trfvtype + '_best_LP_ANN.pkl'
    # save the optimized model
    joblib.dump(best_ANN, savename)

    # store F-score to the list to summarize the results at the end
    list_fscore = list_fscore.append(pd.DataFrame(index=[trfvtype + '_LP_ANN'], data=[best_ANN_scores_df.mean()]))

    if plot:
        figname = savepth + 'figs/' + trfvtype + '_best_LP_ANN'
        plot_opt([*best_ANN_param], trials_ANN, figname)

    ## =========================== Support Vector Machine ========================== #
    if verbose:
        print('optimizing Support Vector Machine...')
    # Find the best model by optimizing hyperparameters
    best_SVM_temp, trials_SVM = optimize_model(objective_LP_SVM, param_space('SVM', fv_train.shape[1]), MAX_EVALS)
    # Convert parameter space index to actual parameter space
    best_SVM_param = space_eval(param_space('SVM', fv_train.shape[1]), best_SVM_temp)
    # Evaluate the best model
    best_SVM = LabelPowerset(SVC(**best_SVM_param,
                                   random_state=0))
    best_SVM_scores = model_selection.cross_validate(best_SVM,
                                                     fv_train,
                                                     class_train,
                                                     cv=cv,
                                                     scoring=scoring,
                                                     return_train_score=False,
                                                     n_jobs=-1)

    # cross-validation results (dict) to dataframe
    best_SVM_scores_df = pd.DataFrame.from_dict(best_SVM_scores).drop(['fit_time', 'score_time'], axis=1)

    if verbose:
        print('done.. \n{} is {}'.format(scoring, best_ANN_scores_df.mean()['test_score']))

    # set save filename
    savename = savepth + 'models/' + trfvtype + '_best_LP_SVM.pkl'
    # save the optimized model
    joblib.dump(best_SVM, savename)

    # store Rsquare to the list to summarize the results at the end
    list_fscore = list_fscore.append(pd.DataFrame(index=[trfvtype + '_LP_SVM'], data=[best_SVM_scores_df.mean()]))

    if plot:
        figname = savepth + 'figs/' + trfvtype + '_best_LP_SVM'
        plot_opt([*best_SVM_param], trials_SVM, figname)


# set savename for cross-validation scores
savename = savepth + 'summary_{}_LP.csv'.format(scoring)
# save cross-validation scores
list_fscore.to_csv(savename)




