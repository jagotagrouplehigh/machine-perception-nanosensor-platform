""" This script reads the test feature vectors (patient samples) and trained classification/regression models,
predict the results, then return the prediction probability """

""" Import Libraries """
from glob import glob
import pandas as pd
from sklearn.externals import joblib
from pathlib import Path

verbose = True

def readfv(fvtype, datanames, clf=True, cutoff=[0.01, 0.01, 0.01]):
    """ Return proper feature vector and class for training set and feature vector for test set
        fvtype: feature vector type to find input data filename
        datanames: datanames set
        clf: if True, return class not concentration """

    # read input data that includes the specified feature vector type
    dname = [s for s in datanames if fvtype in s][0]
    df = pd.read_csv(dname)

    """ Split data into features and classes """
    df_temp = df
    # index of column for class (label)
    idx_class = [i for i, s in enumerate(df.columns) if '_' in s][-1]+1
    fv_train = df_temp.iloc[:, 0:idx_class]

    # normalize the columns where each value is between -1 and 1
    fv_train = fv_train.apply(lambda x: (x - x.min()) / (x.max() - x.min()) * 2 - 1, axis=0)
    fv_train.fillna(value=0, inplace=True)

    # extract class (label) only
    class_train = df_temp[['HE4', 'CA125', 'YKL40']]

    if verbose:
        print('feature vectors and classes(labels) were extracted from the file ({})'.format(dname))

    if clf:
        cutoff_HE4, cutoff_CA125, cutoff_YKL40 = cutoff

        # set classes based on the cutoff concentration
        class_train['HE4'].values[class_train['HE4'].values < cutoff_HE4] = 0
        class_train['HE4'].values[class_train['HE4'].values >= cutoff_HE4] = 1
        class_train['CA125'].values[class_train['CA125'].values < cutoff_CA125] = 0
        class_train['CA125'].values[class_train['CA125'].values >= cutoff_CA125] = 1
        class_train['YKL40'].values[class_train['YKL40'].values < cutoff_YKL40] = 0
        class_train['YKL40'].values[class_train['YKL40'].values >= cutoff_YKL40] = 1

        if verbose:
            print('classes are defined based on the cutoff concentrations \n'
                  '( HE4: {},    CA125: {},    YKL40: {} )'.format(cutoff_HE4, cutoff_CA125, cutoff_YKL40))

        # set class variable tyte as int
        class_train = class_train.astype('int')

    return fv_train, class_train


def test_model(filename, datanames, ctrl):
    """ Predict the results using the trained model and return the prediction probability)
        filename: model filename
        datanames: list of the input data filenames
        ctrl: control sample type: PBS or pat (patient) """

    # extract the feature vector type and model algorithm from the filename
    md_detail = filename.split('/')[-1].split('.')[0].split('best_')[-1]
    tr_detail = filename.split('/')[-1].split('.')[0].split('_best')[0]

    # set path to save the feature importance results
    savepth = '{}/test_{}ctrl'.format(filename.split('/models')[0], ctrl)
    Path(savepth).mkdir(parents=True, exist_ok=True)

    # extract input data filenames that have only training data
    trdatanames = [nm for nm in datanames if not 'ctrl' in nm]
    # extract feature vector and classes (or concentrations)
    if 'reg' in filename:
        fv_train, class_train = readfv(tr_detail, trdatanames, clf=False)
    else:
        fv_train, class_train = readfv(tr_detail, trdatanames, clf=True, cutoff=[0.1, 0.1, 0.1])

    # test input data filenames for a given control sample type
    fname_tst = [nm for nm in datanames if (ctrl in nm) and (tr_detail in nm)][0]
    fv_tst, class_tst = readfv(fname_tst, datanames, clf=False)
    # In test set, there's no concentration information and we have only patient index, that is,
    # the class here is the patient index (Pid) not the actual concentration of analytes.
    # So, let us drop the fake analyte class columns and change the column name to 'Pid'.
    class_tst = class_tst.drop(['CA125', 'YKL40'], axis=1)
    class_tst.columns = ['Pid']

    # read model
    clf = joblib.load(filename)

    # set the SVM model to return prediction probability
    if 'SVM' in filename:
        if 'reg' in filename:
            clf.probability = True
        else:
            if 'BR' in str(md_detail) or 'LP' in str(md_detail):
                clf.classifier.probability = True
            else:
                clf.probability = True

    # fit the model and estimate the feature importance using the model
    clf = clf.fit(fv_train, class_train)

    # calculate the prediction probability
    try:
        class_pred = clf.predict_proba(fv_tst)
    except:
        print('Regression does not provide prediction probability.')
        class_pred = clf.predict(fv_tst)

    if 'reg' in filename:
        for i in range(0, 3):
            colnm = class_train.columns[i]
            class_tst[colnm] = class_pred[:, i]
    else:
        if 'BR' in str(md_detail) or 'LP' in str(md_detail):
            class_pred = class_pred.todense()

        for i in range(0, 3):
            colnm = class_train.columns[i]
            if 'BR' not in str(md_detail) and 'LP' not in str(md_detail):
                if 'RF' in str(md_detail):
                    class_tst[colnm] = class_pred[i][:, 1]
                else:
                    class_tst[colnm] = class_pred[:, i]
            else:
                class_tst[colnm] = class_pred[:, i]

    # rearrange the predict results by the patient id
    class_tst = class_tst.sort_values(by=['Pid'])

    # calculate the average of prediction probability for each patient
    class_tst_mean = class_tst.groupby('Pid').mean().reset_index()

    # set savename
    savename = savepth + '/predprob_' + tr_detail + '_' + md_detail + '.csv'
    class_tst.to_csv(savename)
    class_tst_mean.to_csv(savename.replace('pred', 'mean_pred'))

    return




""" ======================================== main script ======================================== """

# List filenames
filenames = glob('../results/multi*/models/*.pkl')
datanames = glob('../data/fv*')

# control sample types
ctrls = ['pat', 'PBS']
for ctrl in ctrls:
    for filename in filenames:
        test_model(filename, datanames, ctrl)


