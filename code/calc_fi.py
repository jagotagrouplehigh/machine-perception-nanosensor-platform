""" This script reads the feature vectors and trained Random Forest classification/regression models,
then calculate the feature importance """

""" Import Libraries """
from glob import glob
import pandas as pd
from sklearn.externals import joblib
from pathlib import Path

verbose = True

def readfv(fvtype, datanames, clf=True, cutoff=[0.01, 0.01, 0.01]):
    """ Return proper feature vector and class for training set and feature vector for test set
        fvtype: feature vector type to find input data filename
        datanames: datanames set
        clf: if True, return class not concentration """

    # read input data that includes the specified feature vector type
    dname = [s for s in datanames if fvtype in s][0]
    df = pd.read_csv(dname)

    """ Split data into features and classes """
    df_temp = df
    # index of column for class (label)
    idx_class = [i for i, s in enumerate(df.columns) if '_' in s][-1]+1
    fv_train = df_temp.iloc[:, 0:idx_class]

    # normalize the columns where each value is between -1 and 1
    fv_train = fv_train.apply(lambda x: (x - x.min()) / (x.max() - x.min()) * 2 - 1, axis=0)

    # extract class (label) only
    class_train = df_temp[['HE4', 'CA125', 'YKL40']]

    if verbose:
        print('feature vectors and classes(labels) were extracted from the file ({})'.format(dname))

    if clf:
        cutoff_HE4, cutoff_CA125, cutoff_YKL40 = cutoff

        # set classes based on the cutoff concentration
        class_train['HE4'].values[class_train['HE4'].values < cutoff_HE4] = 0
        class_train['HE4'].values[class_train['HE4'].values >= cutoff_HE4] = 1
        class_train['CA125'].values[class_train['CA125'].values < cutoff_CA125] = 0
        class_train['CA125'].values[class_train['CA125'].values >= cutoff_CA125] = 1
        class_train['YKL40'].values[class_train['YKL40'].values < cutoff_YKL40] = 0
        class_train['YKL40'].values[class_train['YKL40'].values >= cutoff_YKL40] = 1

        if verbose:
            print('classes are defined based on the cutoff concentrations \n'
                  '( HE4: {},    CA125: {},    YKL40: {} )'.format(cutoff_HE4, cutoff_CA125, cutoff_YKL40))

        # set class variable tyte as int
        class_train = class_train.astype('int')

    return fv_train, class_train


def calculate_fi(filename, datanames):
    """ Calculate feature importance of the models trained using Random Forest algorithm
        filename: model filename
        datanames: list of the input data filenames """

    # extract the feature vector type and model algorithm from the filename
    md_detail = filename.split('/')[-1].split('.')[0].split('best_')[-1]
    tr_detail = filename.split('/')[-1].split('.')[0].split('_best')[0]

    if 'RF' in str(md_detail):
        if 'BR' in str(md_detail) or 'LP' in str(md_detail):
            print('Feature importance is not available for {}'.format(md_detail))
            return
        else:
            # set path to save the feature importance results
            savepth = filename.split('/models')[0]
            Path(savepth + '/fi').mkdir(parents=True, exist_ok=True)

            # extract feature vector and classes (or concentrations)
            if 'reg' in filename:
                fv_train, class_train = readfv(tr_detail, datanames, clf=False)
            else:
                fv_train, class_train = readfv(tr_detail, datanames, clf=True, cutoff=[0.1, 0.1, 0.1])

            # read model
            clf = joblib.load(filename)

            # fit the model and estimate the feature importance using the model
            clf = clf.fit(fv_train, class_train)

            # create dataframe to save feature importance
            FI = pd.DataFrame(clf.feature_importances_,
                              index=fv_train.columns,
                              columns=['importance'])
            # set savename
            savename = savepth + '/fi/fi_' + tr_detail + '_' + md_detail + '.csv'
            # save feature importance
            FI.to_csv(savename)

    else:
        print('Feature importance is not available for {}'.format(md_detail))

    return

""" ======================================== main script ======================================== """

# List filenames
filenames = glob('../results/multi*/models/*.pkl')
datanames = list(set(glob('../data/fv*')) - set(glob('../data/*ctrl*')))

# calculate the feature importance and save the results
for filename in filenames:
    calculate_fi(filename, datanames)


